#!/usr/bin/env bash

ROOT_DIR="${HOME}/.silhouette"

git clone git@github.com:mindmorass/silhouette.git $ROOT_DIR

cd $ROOT_DIR
echo "The first profile build will take a few minutes"
echo "Subsequent runs will be much faster"
echo
echo "Are you ready to continue? (y/n)"
read a

if [ "${a}" == "y" ]; then
    ansible-playbook main.yml
fi

exit 0
