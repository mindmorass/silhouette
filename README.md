# silhouette

## Current State

proof of concept

## Install

```
bash -c "$(curl -fsSL https://gitlab.com/mindmorass/silhouette/raw/master/roles/silhouette/files/install.sh)"
```

## TODO

- make task idempotent
- profile build per desired distribution
- dockerize tests
- build pipeline with gitlab.com
- break out roles into their own modules istead of this monolithic mess
- wherever possible, move away from templates and move inline file edits
